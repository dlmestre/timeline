from PIL import Image, ImageDraw, ImageFont
import pandas as pd
import re

white = (255, 255, 255)
new_color = (200,200,200)
black = (0, 0, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
green = (0,128,0)

class TimeLine:
    def __init__(self,dataframe,date_column,text_column,image,background_color,color,font_size):
        self.df = dataframe
        self.Dates = self.df[date_column]
        self.Notes = self.df[text_column]
        self.image_file = image
        self.background_color = background_color
        self.color = color
        
        #sets the color used on the Date
        self.date_color = red
        
        #file's width
        self.width = 2460
        
        #size limit for each line
        self.limit = self.width / 5
        self.spacingX = 40
        self.encoding = "UTF-8"
        
        #absolute path to font file
        self.font_path = 'arial.ttf'
        self.font = ImageFont.truetype(self.font_path, font_size, encoding=self.encoding)
        self.get_years()
        self.height = self.size()
        self.create_image()
    #finds the year - range 1900-2099
    def year_regex(self,string):
        regex = r"(19|20[0-9]{2})"
        return re.findall(regex,string)[0]
    #creates a list of years
    def get_years(self):
        try:
            self.years = self.Dates.apply(lambda x: self.year_regex(x)).tolist()
        except Exception as e:
            print(e)
            self.years = None
    #splits the text in different lines, if the text exceeds the width (a given variable in pixels), 
    #it creates a new line
    def wrap_text(self, text, width):
        text_lines = []
        text_line = []
        text = text.replace('\n', ' [br] ')
        words = text.split()
        font_size = self.font.getsize(text)

        for word in words:
            if word == '[br]':
                text_lines.append(' '.join(text_line))
                text_line = []
                continue
            text_line.append(word)
            w, h = self.font.getsize(' '.join(text_line))
            if w > width:
                text_line.pop()
                text_lines.append(' '.join(text_line))
                text_line = [word]

        if len(text_line) > 0:
            text_lines.append(' '.join(text_line))

        return text_lines
    #measures the size that will be needed to insert all lines
    def size(self):
        height = 100
        
        date = "Words" 
        self.string_heigth = self.font.getsize(date)[1]

        for Note in self.Notes:
            date = "Words" 
            Lines = [date]
            Lines = Lines + self.wrap_text(Note,self.limit)
            value = 20
            for Line in Lines:
                value += self.spacingX + self.string_heigth / 4
            height += value + self.string_heigth / 2
            height += 120
        height += 80
        return height
    #creates the image
    def create_image(self):
        self.image = Image.new("RGB", (self.width, self.height + 20), self.background_color)
        self.draw = ImageDraw.Draw(self.image)
        
        countX = 0
        valueY = 80
        count_x_even = 0
        count_x_odd = 0
        current_year = ""
            
        y1 = 0
        y2 = self.height + 20
        self.draw.line((self.width/2 + 15, y1, self.width/2 + 15, y2), black,width=40) 

        for Note in self.Notes:
            
            date = self.Dates[countX] 
            
            #date's width will be identical to each line width
            date_width = self.font.getsize(date)
            r = 10
            
            #sets the position of date and text
            if countX % 2 == 0:
                valueLine = float(date_width[0]) * 1.3
                if count_x_even %2 == 0:
                    valueX = self.width/8
                    #draws date
                    self.draw.text((valueX,valueY),date,self.date_color,font=self.font)
                else:
                    valueX = self.width/6
                    #draws date
                    self.draw.text((valueX,valueY),date,self.date_color,font=self.font)
                count_x_even += 1
            else:
                valueLine = -float(date_width[0]) * 0.35
                if count_x_odd %2 == 0:
                    valueX = self.width/2 + self.width/6
                    #draws date
                    self.draw.text((valueX,valueY),date,self.date_color,font=self.font)
                else:
                    valueX = self.width/2 + self.width/6 + 150
                    #draws date
                    self.draw.text((valueX,valueY),date,self.date_color,font=self.font)
                count_x_odd += 1
            
            #create circles
            self.draw.ellipse((valueLine+valueX-r, valueY-r,valueLine+valueX+r, valueY+r),fill=black)
            
            #guards the Y position to use later on
            valueYLineYear = valueY
            
            #draws the each line
            self.draw.line((valueLine+valueX, valueY, self.width/2, valueY), black,width=10) 
            
            valueY += 40 + self.string_heigth / 4
            Lines = self.wrap_text(Note,self.limit)

            value = 20

            for Line in Lines:
                #draws each line
                self.draw.text((valueX, valueY + value),Line,self.color,font=self.font)
                value += self.spacingX + self.string_heigth / 4
            
            #if we have a list of "years"
            if self.years:
                YearFont = ImageFont.truetype(self.font_path, 50, encoding=self.encoding)
                tmp = current_year
                #we iterate each year on the list
                current_year = self.years[countX]
                #if the current year is different from the previous year (tmp variable)
                if current_year != tmp:
                    #we split each character ont the current_year variable
                    year_split = [char for char in current_year]
                    val = 0
                    for char in current_year:
                        #draws each character
                        self.draw.text((self.width / 2,valueYLineYear + val),char,white,font=YearFont)
                        val += 50
            valueY += value + self.string_heigth / 2
            countX += 1
            valueY += 120  

        self.image.save(self.image_file)

path = "testfile.csv"

df = pd.read_csv(path)

obj = TimeLine(df,"ndate","notetext","another_image13.png",white,black,45)
